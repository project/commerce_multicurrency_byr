<?php

/**
 * Fetch the currency exchange rates for the requested currency combination.
 * Use nbrb.by as provider.
 *
 * Return an array with the array(target_currency_code => rate) combination.
 *
 * @param string $currency_code
 *   Source currency code.
 * @param array $target_currencies
 *   Array with the target currency codes.
 *
 * @return array
 *   Array with the array(target_currency_code => rate) combination.
 */
function commerce_multicurrency_byr_exchange_rate_sync_provider_nbrb($currency_code, $target_currencies) {
  $data = cache_get(__FUNCTION__, 'cache');

  if (!$data) {
    $nbrb_rates = array();
    if (($xml = @simplexml_load_file('http://www.nbrb.by/Services/XmlExRates.aspx')) && @count($xml->Currency)) {
      foreach ($xml->Currency as $item) {
        $rate = (float) str_replace(',', '.', (string) $item->Rate);
        $nbrb_rates[(string) $item->CharCode] = empty($rate) ? 0 : $item->Scale / $rate;
      }
      cache_set(__FUNCTION__, $nbrb_rates, 'cache', time() + 3600);
    }
    else {
      watchdog(
        'commerce_multicurrency', 'Rate provider nbrb.by: Unable to fetch / process the currency data of @url',
        array('@url' => 'http://www.nbrb.by/Services/XmlExRates.aspx'),
        WATCHDOG_ERROR
      );
    }
  }
  else {
    $nbrb_rates = $data->data;
  }

  $rates = array();
  foreach ($target_currencies as $target_currency_code) {
    if ($currency_code == 'BYR' && isset($nbrb_rates[$target_currency_code])) {
      $rates[$target_currency_code] = $nbrb_rates[$target_currency_code];
    }
    elseif (isset($nbrb_rates[$currency_code]) && $target_currency_code == 'BYR') {
      $rates[$target_currency_code] = 1 / $nbrb_rates[$currency_code];
    }
    elseif (isset($nbrb_rates[$currency_code]) && isset($nbrb_rates[$target_currency_code])) {
      $rates[$target_currency_code] = $nbrb_rates[$target_currency_code] / $nbrb_rates[$currency_code];
    }
  }

  return $rates;
}
